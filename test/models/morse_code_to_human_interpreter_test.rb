require 'test_helper'

class MorseCodeToHumanInterpreterTest < ActiveSupport::TestCase
  
  def setup
    @interpreter = MorseCodeToHumanInterpreter.new(' ', '', '  ', ' ')
  end
  
  test "interpret a letter to human" do
    assert (@interpreter.interpret('....')) == 'H'
  end
  
  test "interpret two letter to human" do
    assert (@interpreter.interpret('.... ---')) == 'HO'
  end
  
  test "interpret two letter separated by a space to human" do
    assert (@interpreter.interpret('....  ---')) == 'H O'
  end
  
  test "interpret Morse to Human" do
    assert (@interpreter.interpret('.... --- .-.. .-  -- . .-.. ..')) == 'HOLA MELI'
  end
  
  test "interpret Morse to Human with customized separators" do
    interpreter = MorseCodeToHumanInterpreter.new('  ', '', '   ', ' ')

    assert interpreter.interpret('....  ---  .-..  .-   --  .  .-..  ..') == 'HOLA MELI'
  end
end
