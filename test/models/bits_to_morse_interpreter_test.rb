require 'test_helper'

class BitsToMorseInterpreterTest < ActiveSupport::TestCase
  def setup
    @interpreter = BitsToMorseInterpreter.new('0000', ' ', ' ', '')
  end
  
  test "interpret a bit chain to a morse letter" do
    assert (@interpreter.interpret('11011011011')) == '....'
  end
  
  test "interpret a bit chain to two morse letters" do
    assert (@interpreter.interpret('0000000011011011001110000011111100011111100111111')) == '.... ---'
  end
  
  test "interpret a bit chain to two letter separated by a space in morse code" do
    assert (@interpreter.interpret('000000001101101100111000001111110001111110011111100000001110111111110111011100000001100011111100000111111001111110000000110000110111111110111011100000011011100000000000')) == '.... --- .-.. .- -- . .-.. ..'
  end
end
