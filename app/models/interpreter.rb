class Interpreter
  
  def initialize(letter_separator, decoded_letter_separator, words_separator, decoded_word_separator)
    initialize_base_conversion
    @letter_separator = letter_separator
    @decoded_letter_separator = decoded_letter_separator
    @words_separator = words_separator
    @decoded_word_separator = decoded_word_separator
  end
  
  def interpret(string)
    words = string.split(@words_separator)
    (words.collect {|each| interpret_word(each)}).join(@decoded_word_separator).strip
  end
  
  def interpret_word(string)
    letters = string.split(@letter_separator).select {|each| !each.empty?}
    result = letters.collect { |letter| convert(letter)}
    
    result.join(@decoded_letter_separator)
  end
  
  
  def convert(string)
    fail NotImplementedError, "An Interpreter class must be able to convert a string!"
  end
  
  def initialize_base_conversion
    fail NotImplementedError, "An Interpreter class must be able to initialize your base conversion!"
  end
  
end
