class BitsToMorseInterpreter < Interpreter

  def convert(string)
    result = ''
    letters = string.split('0').select {|each| !each.empty?}
    
    letters.each do |letter|
     result += (letter.size <= @base_conversion ? '.' : '-')
    end
    
    result
  end
  
  def initialize_base_conversion
    @base_conversion ||= 3
  end
end
