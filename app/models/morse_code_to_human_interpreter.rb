class MorseCodeToHumanInterpreter < Interpreter

  def convert(string)
    @morse_code[string]
  end
  
  def initialize_base_conversion
    @morse_code ||= {}
    @morse_code['.-'] = 'A'
    @morse_code['-...'] = 'B'
    @morse_code['-.-.'] = 'C'
    @morse_code['-..'] = 'D'
    @morse_code['.'] = 'E'
    @morse_code['..-.'] = 'F'
    @morse_code['--.'] = 'G'
    @morse_code['....'] = 'H'
    @morse_code['..'] = 'I'
    @morse_code['.---'] = 'J'
    @morse_code['-.-'] = 'K'
    @morse_code['.-..'] = 'L'
    @morse_code['--'] = 'M'
    @morse_code['-.'] = 'N'
    @morse_code['---'] = 'O'
    @morse_code['.--.'] = 'P'
    @morse_code['--.-'] = 'Q'
    @morse_code['.-.'] = 'R'
    @morse_code['...'] = 'S'
    @morse_code['-'] = 'T'
    @morse_code['..-'] = 'U'
    @morse_code['...-'] = 'V'
    @morse_code['.--'] = 'W'
    @morse_code['-..-'] = 'X'
    @morse_code['-.--'] = 'Y'
    @morse_code['--..'] = 'Z'
    @morse_code['-----'] = '0'
    @morse_code['.----'] = '1'
    @morse_code['..---'] = '2'
    @morse_code['...--'] = '3'
    @morse_code['....-'] = '4'
    @morse_code['.....'] = '5'
    @morse_code['-....'] = '6'
    @morse_code['--...'] = '7'
    @morse_code['---..'] = '8'
    @morse_code['----.'] = '9'
    @morse_code['.-.-.-'] = 'Fullstop'
  end
end
